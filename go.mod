module code.rocketnine.space/tslocum/godoc-static

go 1.15

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/andybalholm/cascadia v1.2.0 // indirect
	github.com/yuin/goldmark v1.2.1
	golang.org/x/mod v0.3.0
	golang.org/x/net v0.0.0-20200904194848-62affa334b73
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
